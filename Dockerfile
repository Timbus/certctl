FROM alpine:3.8 AS builder
RUN apk add --no-cache libressl-dev musl-dev zlib-dev libevent gc pcre crystal shards

COPY . /app
WORKDIR /app
RUN shards build --release
RUN strip -S /app/bin/certctl

FROM alpine:3.8
RUN apk add --no-cache zlib libgcc libressl libevent gc pcre dumb-init

COPY --from=builder /app/bin/certctl /bin

ENTRYPOINT [ "/usr/bin/dumb-init", "--" ]
CMD [ "certctl" ]
