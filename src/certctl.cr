require "http/client"
require "openssl"
require "json"
require "base64"

class Certctl
  getter cert_name = ENV["CERT_NAME"]

  getter cert_path : String = ENV.fetch("CERT_PATH", "/share/live")
  getter magic_annotation : String = ENV.fetch("MAGIC_ANNOTATION", "stable.k8s.msts/autocert")

  getter kubernetes_url : String = ENV.fetch("KUBERNETES_URL", "kubernetes.default.svc.cluster.local")
  getter kube_cert_path : String = ENV.fetch("KUBERNETES_CERT", "/var/run/secrets/kubernetes.io/serviceaccount/ca.crt")
  getter kube_token_path : String = ENV.fetch("KUBERNETES_TOKEN", "/var/run/secrets/kubernetes.io/serviceaccount/token")
  getter kube_token : String do
    File.read(self.kube_token_path)
  end

  getter ssl_client = OpenSSL::SSL::Context::Client.new
  getter watcher_client : HTTP::Client do
    HTTP::Client.new(kubernetes_url, tls: ssl_client)
  end
  getter api_client : HTTP::Client do
    HTTP::Client.new(kubernetes_url, tls: ssl_client)
  end

  def initialize
    ssl_client.ca_certificates = kube_cert_path
    watcher_client.before_request do |request|
      request.headers["Authorization"] = "Bearer #{kube_token}"
    end
    api_client.before_request do |request|
      request.headers["Authorization"] = "Bearer #{kube_token}"
      request.headers["Content-Type"] = "application/json"
    end
  end

  def watch_ingress(channel)
    loop do
      puts "Watcher connecting to kube"
      uri = URI.new
      uri.path = "/apis/extensions/v1beta1/watch/ingresses"
      uri.query = HTTP::Params.encode({"timeoutSeconds" => 1.day.to_i.to_s})
      watcher_client.get(uri.full_path) do |response|
        unless response.success?
          puts "Got bad response from kubernetes -> '#{response.status_code}: #{response.status_message}'"
          puts "Shutting down to force retry.."
          exit 1
        end
        while line = response.body_io.gets
          channel.send(line)
        end
      end
    rescue # Discard connection errors
    ensure
      sleep 10
    end
  end

  def main
    channel = Channel(String).new
    spawn watch_ingress(channel)

    loop do
      json_string = channel.receive
      json = JSON.parse(json_string)

      case (json["type"])
      when "ADDED", "MODIFIED"
        next unless json["object"]["kind"] == "Ingress"
        ingress_name = json["object"]["metadata"]["name"]
        namespace = json["object"]["metadata"]["namespace"]
        puts "Inspecting ingress '#{namespace}/#{ingress_name}'"

        next unless json["object"]["spec"]["tls"]?
        next unless json["object"]["metadata"]["annotations"]?.try &.[magic_annotation]?

        secret_names = json["object"]["spec"]["tls"].as_a.compact_map &.["secretName"]?

        tls_cert = Base64.strict_encode(File.read("#{cert_path}/#{cert_name}/fullchain.pem"))
        tls_key = Base64.strict_encode(File.read("#{cert_path}/#{cert_name}/privkey.pem"))

        payload = {
          "apiVersion": "v1",
          "kind":       "Secret",
          "type":       "kubernetes.io/tls",
          "data":       {"tls.crt": tls_cert, "tls.key": tls_key},
        }

        secret_names.each do |secret_name|
          named_payload = payload.merge({"metadata": {"name": secret_name}})
          # Update secret, if possible.
          result = api_client.put(
            "/api/v1/namespaces/#{namespace}/secrets/#{secret_name}",
            body: named_payload.to_json,
          )
          if (result.status_code == 404)
            # If not found, try to create a new secret.
            result = api_client.post(
              "/api/v1/namespaces/#{namespace}/secrets/",
              body: named_payload.to_json,
            )
          end
          if result.success?
            puts "Added secret '#{secret_name}' for ingress '#{namespace}/#{ingress_name}'"
          else
            puts "Failed to create/update secret for ingress '#{namespace}/#{ingress_name}':", result.body
          end
        end
      when "DELETED"
        # ...
      end
    rescue ex : KeyError | JSON::ParseException # Ignore JSON errors and move on
      puts "Error handling JSON: #{ex.message}"
    end
  end
end

# Run main
Certctl.new.main
